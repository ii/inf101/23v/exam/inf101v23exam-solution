[Oppgave 3: Whack-a-mole misclick](./06-OPG3-WHACK-A-MOLE.md) &bullet; [README](../README.md)

# Oppgave 4: paint

![Illustrasjon av virkemåten til Paint-programmet](./img/paint.gif)

Lag et tegneprogram som vist over. Benytt model-view-controller for å skille variabler som beskriver programmets tilstand (modellen) fra kode som viser programmets tilstand på skjermen (visning) og kode som håndterer hendelser (kontroller). La all koden du skriver for denne oppgaven ligge i pakken exam23v.paint og eventuelle underpakker (men du kan selvsagt importere klasser fra andre deler av start-repoet).

Hint: ta gjerne inspirasjon fra Tetris, Whack-a-mole fra forrige oppgave og https://git.app.uib.no/ii/inf101/23v/students/clickablegrid. Bruk gjerne internett/hjelpemidler/AI og søk etter løsninger. Husk (som alltid) å sitere kilder, inkludert alle avsnitt på tre eller flere sammenhengende kodelinjer som er produsert av en AI.

Programmet ditt trenger kun å tegne bilder som 200x200 piksler, men bildestørrelsen bør være lett å endre på senere.


Du vil bli bedømt i følgende kategorier. Underpunktene i hver kategori er rangert etter hvor viktige de er for bedømmelsen.

**Funksjonalitet**
- man kan klikke på en piksel for å fargelegge den (ca 30%)
- man kan velge hvilken farge man skal tegne med (ca 30%)
- man kan se hvilken farge som er valgt (ca 10%)
- man kan nullstille tegningen (ca 10%)
- man kan dra musen når den er nedtrykket for å fargelegge (de fleste) pikslene den passerer (ca 10%)
- man kan lagre tegningen som en fil (ca 10%)

**Modularitet og kodestil** (kan redusere totalscore med opptil 40%  av score oppnådd på funksjonalitet)
- programmet benytter model-view-controller og har ellers god modularitet
- selvdokumenterende kode
- innkapsling
- dokumentasjon av public metoder
- se forøvrig https://inf101.ii.uib.no/notat/stil/

**Testing** (kan redusere totalscore med opptil 20% av score oppnådd for funksjonalitet)
- det er skrevet meningsfulle tester for de metoder i modellen som kalles av kontrolleren


**Helhetsvurdering** (sensor kan redusere eller øke score basert på skjønn dersom rubrikken ikke samsvarer med hvilket nivå kandidatene demonstrerer)



Under finner du et UML-diagram for en eksempel-løsning. Kun enkelte public metoder og relasjoner er vist. Merk at eksempel-løsningen benyttet en EventBus for kommunikasjon, men denne delen av programmet er ikke tegnet inn for å gjøre tegningen mer oversiktlig. Du velger selv om du ønsker å følge diagrammet nøyaktig, om du vil gjøre dine egne tilpasninger, eller om du vil løse det på en annen måte.

![UML -diagram for eksempelløsning](./img/uml-paint-sample.png)


Dersom du ikke blir ferdig, kan det være lurt å kommentere ut kode som fører til kompileringsfeil, slik at det du leverer inn faktisk kan kjøres. Da blir sensor glad i deg!