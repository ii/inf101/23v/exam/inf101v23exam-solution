[README](../README.md) &bullet; [Dokumentasjon ved bruk av AI](./02-DOKUMENTASJON-AI.md)
# Egenerklæring

Jeg erklærer herved at besvarelsen som jeg leverer er mitt eget arbeid.

Jeg har ikke:
 - samarbeidet med andre
 - brukt andres arbeid uten at dette er oppgitt, inkludert arbeid utført av kunstig intelligens.
 - brukt eget tidligere arbeid (innleveringer/ eksamenssvar) uten at dette er oppgitt

Om jeg har benyttet litteratur *ut over kursnotatene*, vil all bruk være tydelig markert i kildekoden og/eller i tekstbesvarelsen hvor det benyttes.

**Jeg er kjent med at brudd på disse bestemmelsene er å betrakte som fusk og kan føre til annullert eksamen og/eller utestengelse.**

Dersom du er usikker på om du kan stille deg bak erklæringen, se [retningslinjer for bruk av kilder i skriftlige arbeider ved Universitetet i Bergen](https://www.uib.no/student/49084/l%C3%A6r-deg-%C3%A5-bruke-kilder-riktig).

Alle eksamensbesvarelser ved UiB blir sendt til manuell og elektronisk plagiatkontroll.


*Merk: Ved å fortsette bekrefter jeg at jeg har lest erklæringen og at besvarelsen jeg leverer under denne eksamenen er mitt eget arbeid (og bare mitt eget arbeid), i full overensstemmelse med ovennevnte erklæringen.*

