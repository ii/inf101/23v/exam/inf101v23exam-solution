package no.uib.inf101.exam23v.paint;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.grid.CellPosition;

public class TestPaintModel {
  
  @Test
  public void sanityTest() {
    EventBus eventBus = new EventBus();
    PaintModel model = new PaintModel(eventBus);

    // Initial state: white canvas, black pen
    assertEquals(Color.WHITE, model.getCanvas().get(new CellPosition(42, 16)));
    assertEquals(Color.WHITE, model.getCanvas().get(new CellPosition(199, 199)));
    assertEquals(Color.BLACK, model.getPenColor());

    // Draw on (42, 16) and verify pixel (and only that pixel) changed color
    model.draw(new CellPosition(42, 16));
    assertEquals(Color.BLACK, model.getCanvas().get(new CellPosition(42, 16)));
    assertEquals(Color.WHITE, model.getCanvas().get(new CellPosition(199, 199)));

    // Change color to red and draw (42, 16) again
    model.setPenColor(Color.RED);
    assertEquals(Color.RED, model.getPenColor());

    model.draw(new CellPosition(42, 16));
    assertEquals(Color.RED, model.getCanvas().get(new CellPosition(42, 16)));
    assertEquals(Color.WHITE, model.getCanvas().get(new CellPosition(199, 199)));
  }

  @Test
  public void testClear() {
    EventBus eventBus = new EventBus();
    PaintModel model = new PaintModel(eventBus);

    model.draw(new CellPosition(0, 0));
    model.draw(new CellPosition(199, 199));
    assertEquals(Color.BLACK, model.getCanvas().get(new CellPosition(0, 0)));
    assertEquals(Color.BLACK, model.getCanvas().get(new CellPosition(199, 199)));

    model.clear();
    assertEquals(Color.WHITE, model.getCanvas().get(new CellPosition(0, 0)));
    assertEquals(Color.WHITE, model.getCanvas().get(new CellPosition(199, 199))); 
  }
}