package no.uib.inf101.exam23v.paint.controller;

import no.uib.inf101.exam23v.paint.PaintModel;
import no.uib.inf101.exam23v.paint.view.ViewCanvas;
import no.uib.inf101.grid.CellPosition;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * Controller reacting to mouse clicks and drags in the canvas.
 */
public class ControllerCanvas extends MouseAdapter {
  
  private PaintModel model;
  private ViewCanvas view;

  /**
   * Create a new controller for mouse events on the canvas.
   * 
   * @param model the model to update
   * @param view the view to get mouse events from
   */
  public ControllerCanvas(PaintModel model, ViewCanvas view) {
    this.model = model;
    this.view = view;
    this.view.addMouseListener(this);
    this.view.addMouseMotionListener(this);
  }

  @Override
  public void mousePressed(MouseEvent event) {
    this.drawPoint(event.getPoint());
  }

  @Override
  public void mouseDragged(MouseEvent event) {
    this.drawPoint(event.getPoint());
  }

  private void drawPoint(Point2D point) {
    CellPosition cp = this.view.getCellPositionForPoint(point);
    if (cp == null) {
      return;
    }
    this.model.draw(cp);
  }
}
