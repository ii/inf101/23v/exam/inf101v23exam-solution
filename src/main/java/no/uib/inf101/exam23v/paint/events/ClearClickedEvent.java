package no.uib.inf101.exam23v.paint.events;

import no.uib.inf101.eventbus.Event;

/** 
 * An event sent from view to controller to indicate that
 * the user has clicked the "Clear" button.
 */
public record ClearClickedEvent() implements Event {}
