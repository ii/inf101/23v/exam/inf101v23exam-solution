package no.uib.inf101.exam23v.paint;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.paint.controller.ControllerButtonEvents;
import no.uib.inf101.exam23v.paint.controller.ControllerCanvas;
import no.uib.inf101.exam23v.paint.view.ViewMain;

import javax.swing.JFrame;

/**
 * Main class showing the demo for a clickable grid
 */
public class PaintMain {
  /**
   * Main method for the paint program
   * @param args ignored
   */
  public static void main(String[] args) {
    EventBus bus = new EventBus();
    PaintModel model = new PaintModel(bus);
    ViewMain view = new ViewMain(model, bus);
    new ControllerCanvas(model, view.getCanvasView());
    new ControllerButtonEvents(model, bus);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101 Paint");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
