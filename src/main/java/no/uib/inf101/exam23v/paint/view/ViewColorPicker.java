package no.uib.inf101.exam23v.paint.view;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.paint.PaintModel;
import no.uib.inf101.exam23v.paint.events.ColorClickedEvent;
import no.uib.inf101.graphics.CellPositionToPixelConverter;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridDimension;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.Objects;

/**
 * A panel that displays a color picker.
 * When the user clicks on a color, a {@link ColorClickedEvent} is
 * posted on the event bus with the color that was clicked.
 */
public class ViewColorPicker extends JPanel {

  private static final int OUTER_MARGIN = 10;
  private static final int INNER_MARGIN = 5;
  private static final int PREF_WIDTH = 200;
  private static final int PREF_HEIGHT = 50;

  private final ViewablePaintModel model;

  /**
   * Create a new color picker.
   * 
   * @param model the model containing information about the current
   *              pen color
   * @param eventBus the event bus to post events on when a color is
   *                 clicked
   */
  public ViewColorPicker(ViewablePaintModel model, EventBus eventBus) {
    this.model = model;
    this.setPreferredSize(new Dimension(PREF_WIDTH, PREF_HEIGHT));
    this.setupMouseClicks(eventBus);
  }

  private void setupMouseClicks(EventBus eventBus) {
    this.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        CellPositionToPixelConverter converter = getConverter();
        CellPosition pos = converter.getCellPositionOfPoint(e.getPoint());
        if (pos != null) {
          Color color = PaintModel.COLORS.get(pos.col());
          eventBus.post(new ColorClickedEvent(color));
        }
      }
    });
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawColorButtons(g2);
  }

  private CellPositionToPixelConverter getConverter() {
    return new CellPositionToPixelConverter(
        new Rectangle2D.Double(
            OUTER_MARGIN,
            OUTER_MARGIN,
            this.getWidth() - OUTER_MARGIN * 2,
            this.getHeight() - OUTER_MARGIN * 2),
      new GridDimension.Record(1, PaintModel.COLORS.size()), 
      INNER_MARGIN);
  }

  private void drawColorButtons(Graphics2D g2) {
    CellPositionToPixelConverter converter = getConverter();
    for (int i = 0; i < PaintModel.COLORS.size(); i++) {
      Color color = PaintModel.COLORS.get(i);
      g2.setColor(color);
      Rectangle2D box = converter.getBoundsForCell(new CellPosition(0, i));
      g2.fill(box);
      
      if (Objects.equals(model.getPenColor(), color)) {
        g2.setColor(Color.BLACK);
        g2.draw(box);
        g2.setColor(Color.WHITE);
        g2.draw(new Rectangle2D.Double(
            box.getX() + 1,
            box.getY() + 1,
            box.getWidth() - 2,
            box.getHeight() - 2)
        );
      }
    }
  }
  

}
