package no.uib.inf101.exam23v.paint;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.paint.events.ModelChangedEvent;
import no.uib.inf101.exam23v.paint.view.ViewablePaintModel;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;
import no.uib.inf101.grid.GridDimension;
import no.uib.inf101.grid.IGrid;

import java.awt.Color;
import java.util.List;

/**
 * The model contains two bits of data: the size of the grid to draw, and
 * which cell in the grid is currently highlighted.
 */
public class PaintModel implements ViewablePaintModel {

  public static final List<Color> COLORS = List.of(
      Color.BLACK, Color.RED, Color.GREEN, Color.BLUE,
      Color.YELLOW, Color.CYAN, Color.MAGENTA, Color.WHITE
  );

  private final EventBus bus;
  private final IGrid<Color> canvas;
  private Color penColor = Color.BLACK;

  /**
   * Creates a new PaintModel. Whenever the model changes,
   * it will post a ModelChangedEvent on the given event bus.
   * 
   * @param bus the event bus to use
   */
  public PaintModel(EventBus bus) {
    this.bus = bus;
    this.canvas = new Grid<>(200, 200, Color.WHITE);
  }

  /**
   * Clears the canvas by filling it with white.
   */
  public void clear() {
    this.canvas.fill(Color.WHITE);
    this.bus.post(new ModelChangedEvent());
  }

  /**
   * Sets the current pen color. Will trigger a ModelChangedEvent.
   * 
   * @param color the new pen color
   */
  public void setPenColor(Color color) {
    this.penColor = color;
    this.bus.post(new ModelChangedEvent());
  }

  @Override
  public Color getPenColor() {
    return this.penColor;
  }

  /**
   * Draws a single pixel on the canvas using the current pen color.
   * Will trigger a ModelChangedEvent.
   * 
   * @param pos the position to draw at
   */
  public void draw(CellPosition pos) {
    this.canvas.set(pos, this.penColor);
    this.bus.post(new ModelChangedEvent());
  }

  @Override
  public IGrid<Color> getCanvas() {
    return this.canvas;
  }

  @Override
  public GridDimension getImageDimension() {
    return this.canvas;
  }
}
