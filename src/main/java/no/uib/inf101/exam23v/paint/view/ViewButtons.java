package no.uib.inf101.exam23v.paint.view;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.paint.events.ClearClickedEvent;
import no.uib.inf101.exam23v.paint.events.SaveClickedEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.FlowLayout;

/**
 * A panel containing buttons for clear and save. When the buttons are
 * clicked, respectively a ClearClickedEvent or SaveClickedEvent is
 * posted on the event bus.
 */
public class ViewButtons extends JPanel {

  /**
   * Create a new panel with buttons for clearing the canvas.
   * 
   * @param eventBus the event bus to post events to when buttons are
   *                 clicked
   */
  public ViewButtons(EventBus eventBus) {
    this.setLayout(new FlowLayout());

    JButton clearButton = new JButton("Clear");
    clearButton.addActionListener(e -> eventBus.post(new ClearClickedEvent()));
    this.add(clearButton);

    JButton saveButton = new JButton("Save");
    saveButton.addActionListener(e -> eventBus.post(new SaveClickedEvent()));
    this.add(saveButton);
  }
}
