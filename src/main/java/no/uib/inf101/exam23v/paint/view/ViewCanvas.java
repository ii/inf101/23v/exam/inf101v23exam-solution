package no.uib.inf101.exam23v.paint.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.graphics.CellPositionToPixelConverter;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.GridDimension;

/**
 * A panel drawing the canvas of a paint model.
 */
public class ViewCanvas extends JPanel  {

  private final ViewablePaintModel model;

  /** 
   * Create a new panel drawing the canvas of a paint model.
   * 
   * @param model the model containing the canvas to draw
   */
  public ViewCanvas(ViewablePaintModel model) {
    this.model = model;
    GridDimension dimension = this.model.getImageDimension();
    Dimension dim = new Dimension(dimension.cols(), dimension.rows());
    this.setPreferredSize(dim);
    this.setMaximumSize(dim);
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawPainting(g2);
  }

  private void drawPainting(Graphics2D g2) {
    CellPositionToPixelConverter converter = getConverter();
    for (GridCell<Color> gc : this.model.getCanvas()) {
      Rectangle2D cell = converter.getBoundsForCell(gc.pos());
      g2.setColor(gc.value());
      g2.fill(cell);
    }
  }

  private CellPositionToPixelConverter getConverter() {
    return new CellPositionToPixelConverter(
        new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()),
        this.model.getImageDimension(), 
        0);
  }

  /**
   * Get the coordinate position in the model for a given point in
   * the panel.
   * 
   * @param point the point in the panel
   * @return the corresponding position in the model canvas, or null
   *         if the point is outside the canvas
   */
  public CellPosition getCellPositionForPoint(Point2D point) {
    return this.getConverter().getCellPositionOfPoint(point);
  }
}
