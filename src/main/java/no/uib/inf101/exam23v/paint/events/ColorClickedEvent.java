package no.uib.inf101.exam23v.paint.events;

import java.awt.Color;

import no.uib.inf101.eventbus.Event;

/**
 * An event sent from the view to the controller when a color button
 * is clicked in the GUI.
 * 
 * @param color The color that was clicked.
 */
public record ColorClickedEvent(Color color) implements Event {}
