package no.uib.inf101.exam23v.colorwhack.model;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.exam23v.colorwhack.controller.ControllableWhackModel;
import no.uib.inf101.exam23v.colorwhack.view.ViewableColorWhackModel;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;
import no.uib.inf101.grid.GridCell;
import no.uib.inf101.grid.IReadOnlyGrid;
import no.uib.inf101.grid.eventgrid.EventGrid;

import java.awt.Color;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * A model for a ColorWhack game. This class is responsible for
 * holding the game state and mutating it according to the rules of
 * the game.
 */
public class ColorWhackModel implements ViewableColorWhackModel, ControllableWhackModel {

  // Constants
  private static final int ROWS = 5;
  private static final int COLS = 5;
  private static final int GAME_DURATION_IN_SECONDS = 30;
  private static final int WHACKS_BEFORE_LEVEL_UP = 5;
  private static final int MILLIS_BETWEEN_POPUPS = 400;
  private static final Color EMPYT_COLOR = Color.BLACK;
  private static final List<Color> ALL_COLORS = List.of(
      Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN,
      Color.MAGENTA, Color.ORANGE, Color.PINK
  );

  // Instance variables
  private final Random random;
  private final EventGrid<Color> grid;
  private final EventBus eventBus;
  private LocalDateTime startTime;
  private LocalDateTime lastChangeTime;
  private int numberOfWhacks;
  private GameState gameState;
  private double emptySquareSeconds;

  /**
   * Creates a new ColorWhackModel.
   *
   * @param eventBus the event bus to use
   * @param random  the random number generator to use
   */
  public ColorWhackModel(EventBus eventBus, Random random) {
    Objects.requireNonNull(eventBus, "eventBus cannot be null");
    Objects.requireNonNull(random, "random cannot be null");
    this.eventBus = eventBus;
    this.random = random;
    this.grid = new EventGrid<>(new Grid<>(ROWS, COLS));
    this.reset();
  }

  @Override
  public void reset() {
    this.gameState = GameState.WAITING;
    this.grid.fill(EMPYT_COLOR);
    this.grid.set(new CellPosition(ROWS/2, COLS/2), ALL_COLORS.get(0));
    this.startTime = LocalDateTime.MIN;
    this.lastChangeTime = LocalDateTime.MIN;
    this.emptySquareSeconds = 0;
    this.numberOfWhacks = 0;
    this.eventBus.post(new ModelChangedEvent(this));
  }

  private int countNumberOfEmptySquares() {
    int count = 0;
    for (GridCell<Color> gc : this.grid) {
      if (Objects.equals(gc.value(), EMPYT_COLOR)) {
        count++;
      }
    }
    return count;
  }

  private void accumulateEmptySquareSeconds(LocalDateTime time) {
    Duration duration = Duration.between(this.lastChangeTime, time);
    double seconds = duration.toMillis() / 1000.0;
    this.emptySquareSeconds += seconds * this.countNumberOfEmptySquares();
    this.lastChangeTime = time;
  }

  private void goToActiveGameIfWaiting(LocalDateTime timeStamp) {
    if (Objects.equals(this.gameState, GameState.WAITING)) {
      this.gameState = GameState.RUNNING;
      this.startTime = timeStamp;
      this.lastChangeTime = startTime;
    }
  }

  private Color getNewColor() {
    int bound = Math.min(this.numberOfWhacks / WHACKS_BEFORE_LEVEL_UP + 1, ALL_COLORS.size());
    int index = this.random.nextInt(bound);
    return ALL_COLORS.get(index);
  }

  @Override
  public void setNewColorOnRandomPosition(LocalDateTime timeStamp) {
    Objects.requireNonNull(timeStamp, "timeStamp cannot be null");

    if (Objects.equals(this.gameState, GameState.WAITING)) {
      return;
    }

    this.goToGameOverFromRunningStateIfTimeOut(timeStamp);
    this.accumulateEmptySquareSeconds(timeStamp);

    if (Objects.equals(this.gameState, GameState.RUNNING)) {
      int row = this.random.nextInt(this.grid.rows());
      int col = this.random.nextInt(this.grid.cols());
      this.grid.set(new CellPosition(row, col), this.getNewColor());
    }
    this.eventBus.post(new ModelChangedEvent(this));
  }

  @Override
  public void whack(CellPosition pos, LocalDateTime timeStamp) {
    Objects.requireNonNull(pos, "pos cannot be null");
    Objects.requireNonNull(timeStamp, "timeStamp cannot be null");

    this.goToActiveGameIfWaiting(timeStamp);
    this.goToGameOverFromRunningStateIfTimeOut(timeStamp);
    this.accumulateEmptySquareSeconds(timeStamp);

    if (Objects.equals(this.gameState, GameState.GAME_OVER)) {
      return;
    }

    Color oldColorAtPosition = this.grid.get(pos);
    if (Objects.equals(oldColorAtPosition, EMPYT_COLOR)) {
      fillAllBlankSquaresWithRandomColors();
    } else {
      this.grid.set(pos, EMPYT_COLOR);
    }

    this.numberOfWhacks++;
    this.eventBus.post(new ModelChangedEvent(this));
  }

  private void fillAllBlankSquaresWithRandomColors() {
    for (GridCell<Color> gc : this.grid) {
      if (Objects.equals(gc.value(), EMPYT_COLOR)) {
        this.grid.set(gc.pos(), this.getNewColor());
      }
    }
  }  

  private void goToGameOverFromRunningStateIfTimeOut(LocalDateTime timeStamp) {
    Objects.requireNonNull(timeStamp, "timeStamp cannot be null");

    if (Objects.equals(this.gameState, GameState.RUNNING)) {
      Duration duration = Duration.between(this.startTime, timeStamp);
      if (duration.toSeconds() > GAME_DURATION_IN_SECONDS) {
        this.gameState = GameState.GAME_OVER;
      }
    }
  }

  @Override
  public IReadOnlyGrid<Color> getBoard() {
    return this.grid;
  }

  @Override
  public int getScorePercentage() {
    if (this.emptySquareSeconds <= 0) {
      return 100;
    }

    Duration duration = Duration.between(this.startTime, this.lastChangeTime);
    double secondsSinceStart = duration.toMillis() / 1000.0;
    double squareSeconds = secondsSinceStart * this.grid.rows() * this.grid.cols();
    double percentageCovered = this.emptySquareSeconds / squareSeconds;
    return (int) (100 * percentageCovered);
  }

  @Override
  public int getDelayBetweenPopups(LocalDateTime timeStamp) {
    Objects.requireNonNull(timeStamp, "timeStamp cannot be null");

    if (Objects.equals(this.startTime, LocalDateTime.MIN)) {
      return MILLIS_BETWEEN_POPUPS;
    }
    Duration duration = Duration.between(this.startTime, timeStamp);
    double secondsSinceStart = duration.toMillis() / 1000.0;
    double fractionOfTimeRemaining = Math.max(1 - (secondsSinceStart / GAME_DURATION_IN_SECONDS), 0);
    return (int) (MILLIS_BETWEEN_POPUPS * (fractionOfTimeRemaining / 2.0 + 0.5));
  }

  @Override
  public int getTimeRemaining(LocalDateTime currentTime) {
    Objects.requireNonNull(currentTime, "currentTime cannot be null");

    Duration duration = Duration.between(this.startTime, currentTime);
    double remainingSeconds = GAME_DURATION_IN_SECONDS - duration.toMillis() / 1000.0;
    return Math.max((int) remainingSeconds, 0);
  }

  @Override
  public GameState getGameState() {
    return this.gameState;
  }
}
