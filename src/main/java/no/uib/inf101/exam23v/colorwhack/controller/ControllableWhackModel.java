package no.uib.inf101.exam23v.colorwhack.controller;

import no.uib.inf101.grid.CellPosition;

import java.time.LocalDateTime;

/**
 * A controllable model for a ColorWhack game.
 */
public interface ControllableWhackModel {

  /**
   * Whacks the cell at the given position.
   *
   * @param pos       the position to whack
   * @param timeStamp the time stamp of the whack, typically
   *                  LocalDateTime.now()
   */
  void whack(CellPosition pos, LocalDateTime timeStamp);

  /**
   * Sets a new color on a random position.
   *
   * @param timeStamp the time stamp when the color is set, typically
   *                  LocalDateTime.now()
   */
  void setNewColorOnRandomPosition(LocalDateTime timeStamp);

  /** Resets the game. */
  void reset();

  /**
   * Gets the intended delay between popups.
   *
   * @param timeStamp the time stamp when the delay is calculated,
   *                  typically LocalDateTime.now()
   * @return the delay in milliseconds
   */
  int getDelayBetweenPopups(LocalDateTime timeStamp);

}
